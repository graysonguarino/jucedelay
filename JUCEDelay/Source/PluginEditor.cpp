/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
JUCEDelayAudioProcessorEditor::JUCEDelayAudioProcessorEditor (JUCEDelayAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (800, 500);
    
    timeLSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    timeLSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (timeLSlider);
    
    timeRSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    timeRSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (timeRSlider);
    
    delaySpreadSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    delaySpreadSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (delaySpreadSlider);
    
    feedbackSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    feedbackSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (feedbackSlider);
    
    mixSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    mixSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (mixSlider);
    
    timeLSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "TIME_L", timeLSlider);
    timeRSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "TIME_R", timeRSlider);
    delaySpreadSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "SPREAD", delaySpreadSlider);
    feedbackSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "FEEDBACK", feedbackSlider);
    mixSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "MIX", mixSlider);
}

JUCEDelayAudioProcessorEditor::~JUCEDelayAudioProcessorEditor()
{
}

//==============================================================================
void JUCEDelayAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void JUCEDelayAudioProcessorEditor::resized()
{
    timeLSlider.setBounds (0, getHeight() / 2 - 50, 200, 100);
    timeRSlider.setBounds (timeLSlider.getX() + 125, timeLSlider.getY(), 200, 100);
    delaySpreadSlider.setBounds(timeRSlider.getX() + 125, timeRSlider.getY(), 200, 100);
    feedbackSlider.setBounds (delaySpreadSlider.getX() + 125, timeRSlider.getY(), 200, 100);
    mixSlider.setBounds (feedbackSlider.getX() + 125, feedbackSlider.getY(), 200, 100);
}
