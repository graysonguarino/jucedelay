/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
JUCEDelayAudioProcessor::JUCEDelayAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ), apvts (*this, nullptr, "Parameters", createParameters())
#endif
{
    mPlayingState = false;
    
    apvts.addParameterListener ("TIME_L", this);
    apvts.addParameterListener ("TIME_R", this);
    apvts.addParameterListener ("SPREAD", this);
    apvts.addParameterListener ("FEEDBACK", this);
    apvts.addParameterListener ("MIX", this);
}
JUCEDelayAudioProcessor::~JUCEDelayAudioProcessor()
{
    apvts.removeParameterListener ("TIME_L", this);
    apvts.removeParameterListener ("TIME_R", this);
    apvts.removeParameterListener("SPREAD", this);
    apvts.removeParameterListener ("FEEDBACK", this);
    apvts.removeParameterListener ("MIX", this);
}

//==============================================================================
const juce::String JUCEDelayAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool JUCEDelayAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool JUCEDelayAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool JUCEDelayAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double JUCEDelayAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int JUCEDelayAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int JUCEDelayAudioProcessor::getCurrentProgram()
{
    return 0;
}

void JUCEDelayAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String JUCEDelayAudioProcessor::getProgramName (int index)
{
    return {};
}

void JUCEDelayAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void JUCEDelayAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    mSpec.sampleRate = sampleRate;
    mSpec.numChannels = getTotalNumOutputChannels(); // Should this be input channels?
    mSpec.maximumBlockSize = samplesPerBlock;
    
    delay.reset();
    delay.prepare (mSpec);
    
    delay.setTimeL (*apvts.getRawParameterValue ("TIME_L"));
    delay.setTimeR (*apvts.getRawParameterValue ("TIME_R"));
    delay.setSpread (*apvts.getRawParameterValue ("SPREAD"));
    delay.setFeedback (*apvts.getRawParameterValue ("FEEDBACK"));
    delay.setMix (*apvts.getRawParameterValue ("MIX"));
}

void JUCEDelayAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool JUCEDelayAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void JUCEDelayAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
//    for (int channel = 0; channel < totalNumInputChannels; ++channel)
//    {
//        auto* channelData = buffer.getWritePointer (channel);
//
//
//    }
    
    getPlayHead()->getCurrentPosition (mPlayHeadPos);
    
    if (mPlayHeadPos.isPlaying && !mPlayingState)
        delay.reset();

    mPlayingState = mPlayHeadPos.isPlaying;
    
    delay.applyDelay (buffer);
}

//==============================================================================
bool JUCEDelayAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* JUCEDelayAudioProcessor::createEditor()
{
    return new JUCEDelayAudioProcessorEditor (*this);
    // return new juce::GenericAudioProcessorEditor(*this);
}

//==============================================================================
void JUCEDelayAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void JUCEDelayAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

void JUCEDelayAudioProcessor::parameterChanged (const juce::String& parameterID, float newValue) {
    
    if (parameterID == "TIME_L")
        delay.setTimeL (newValue);
    
    if (parameterID == "TIME_R")
        delay.setTimeR (newValue);
    
    if (parameterID == "SPREAD")
        delay.setSpread (newValue);

    if (parameterID == "FEEDBACK")
        delay.setFeedback (newValue);
    
    if (parameterID == "MIX")
        delay.setMix (newValue);

}

juce::AudioProcessorValueTreeState::ParameterLayout
    JUCEDelayAudioProcessor::createParameters()
{
        juce::AudioProcessorValueTreeState::ParameterLayout params;
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("TIME_L", // ID
                                                               "Time L", // name
                                                               juce::NormalisableRange<float> (0.01f, (float) MAX_DELAY_TIME, 0.01f, 0.5f), // range
                                                               0.1f)); // default value
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("TIME_R", // ID
                                                               "Time R", // name
                                                               juce::NormalisableRange<float> (0.01f, (float) MAX_DELAY_TIME, 0.01f, 0.5f), // range
                                                               0.1f)); // default value
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("SPREAD", // ID
                                                               "Stereo Spread", // name
                                                               juce::NormalisableRange<float> (-1.f, 1.f, 0.01f, 1.f), // range
                                                               1.f)); // default value
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("FEEDBACK",
                                                               "Feedback",
                                                               juce::NormalisableRange<float> (0.f, 1.f, 0.01f),
                                                               .5f));
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("MIX",
                                                               "Mix",
                                                               juce::NormalisableRange<float> (0.f, 1.f, 0.01f),
                                                               .5f));
                                   
        return params;
        
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new JUCEDelayAudioProcessor();
}
