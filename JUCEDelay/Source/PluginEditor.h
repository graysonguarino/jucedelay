/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class JUCEDelayAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    JUCEDelayAudioProcessorEditor (JUCEDelayAudioProcessor&);
    ~JUCEDelayAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    JUCEDelayAudioProcessor& audioProcessor;
    
    juce::Slider timeLSlider;
    juce::Slider timeRSlider;
    juce::Slider delaySpreadSlider;
    juce::Slider feedbackSlider;
    juce::Slider mixSlider;
    
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> timeLSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> timeRSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> delaySpreadSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> feedbackSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> mixSliderAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JUCEDelayAudioProcessorEditor)
};
