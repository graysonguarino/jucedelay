/*
  ==============================================================================

    Delay.h
    Created: 11 Jul 2021 12:58:43pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class Delay
{
public:
    
    Delay();
    ~Delay();
    
    int getMaxDelayTime();
    void prepare (juce::dsp::ProcessSpec spec);
    void reset();
    
    void setTimeL (float newValue);
    void setTimeR (float newValue);
    void setSpread (float newValue);
    void setFeedback (float newValue);
    void setMix (float newValue);
    
    void applyDelay (juce::AudioBuffer<float>& buffer);
    
private:
    
    static const int MAX_DELAY_TIME = 20;
    
    juce::dsp::ProcessSpec mSpec;
    
    float calculateSpread (int inputChannel, int outputChannel);
    
    void applyFeedback (float& leftSample, float& rightSample);
    void applySpread (float& leftSample, float& rightSample);
    
    float addSamples (float firstSample, float secondSample);
    
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mTimeAmtLSmooth; // Left channel
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mTimeAmtRSmooth; // Right channel
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mSpreadAmtSmooth;
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mFeedbackAmtSmooth;
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mMixAmtSmooth;
    
    // TODO - construct mDelayLine in PluginProcessor constructor using getSampleRate() instead of set value
    juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear> mDelayLineL{44100 * MAX_DELAY_TIME};
    juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear> mDelayLineR{44100 * MAX_DELAY_TIME};
};
