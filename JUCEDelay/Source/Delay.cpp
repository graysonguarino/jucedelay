/*
  ==============================================================================

    Delay.cpp
    Created: 11 Jul 2021 12:58:43pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "Delay.h"

Delay::Delay()
{
}

Delay::~Delay()
{
    
}

int Delay::getMaxDelayTime()
{
    return MAX_DELAY_TIME;
}

void Delay::prepare(juce::dsp::ProcessSpec spec)
{
    // jassert(mSpec.numChannels <= 2);
    mSpec = spec;
    juce::dsp::ProcessSpec delayLineSpec;
    delayLineSpec.sampleRate = mSpec.sampleRate;
    delayLineSpec.numChannels = 1;
    
    mDelayLineL.reset();
    mDelayLineL.prepare (delayLineSpec);
    mTimeAmtLSmooth.reset(5000);
    
    mDelayLineR.reset();
    mDelayLineR.prepare(delayLineSpec);
    mTimeAmtRSmooth.reset(5000);
    
    mSpreadAmtSmooth.reset(5000);
    mMixAmtSmooth.reset(5000);
    mFeedbackAmtSmooth.reset(5000);
}

void Delay::reset()
{
    mDelayLineL.reset();
    mDelayLineR.reset();
    mTimeAmtLSmooth.setCurrentAndTargetValue(mTimeAmtLSmooth.getTargetValue());
    mTimeAmtRSmooth.setCurrentAndTargetValue(mTimeAmtRSmooth.getTargetValue());
    mSpreadAmtSmooth.setCurrentAndTargetValue(mSpreadAmtSmooth.getTargetValue());
}

void Delay::setTimeL (float newValue)
{
    mTimeAmtLSmooth.setTargetValue (newValue);
}

void Delay::setTimeR (float newValue)
{
    mTimeAmtRSmooth.setTargetValue (newValue);
}

void Delay::setSpread (float newValue)
{
    mSpreadAmtSmooth.setTargetValue (newValue);
}

void Delay::setFeedback (float newValue)
{
    mFeedbackAmtSmooth.setTargetValue (newValue);
}

void Delay::setMix (float newValue)
{
    mMixAmtSmooth.setTargetValue (newValue);
}

float Delay::calculateSpread (int inputChannel, int outputChannel)
{
    float sameChannelGain = 0.5f * mSpreadAmtSmooth.getNextValue() + 0.5f;
    if (inputChannel == outputChannel)
        return sameChannelGain;
    else
        return .5f - sameChannelGain;
}

void Delay::applySpread(float& leftSample, float& rightSample)
{
    float tempLeftSample = leftSample;
    float tempRightSample = rightSample;
    
    leftSample = (tempLeftSample * calculateSpread(0, 0)) + (tempRightSample * calculateSpread(1, 0));
    rightSample = (tempRightSample * calculateSpread(1, 1)) + (tempLeftSample * calculateSpread(0, 1));
}

float Delay::addSamples (float firstSample, float secondSample)
{
    return (firstSample + secondSample);
}

void Delay::applyDelay (juce::AudioBuffer<float>& buffer)
{
     
    for (int sample = 0; sample < buffer.getNumSamples(); sample++)
    {
        float delayTimeLInSamples = mTimeAmtLSmooth.getNextValue() * mSpec.sampleRate;
        float delayTimeRInSamples = mTimeAmtRSmooth.getNextValue() * mSpec.sampleRate;
        
        mDelayLineL.setDelay (delayTimeLInSamples);
        mDelayLineR.setDelay (delayTimeRInSamples);
        
        float delaySampleL = .0f;
        float delaySampleR = .0f;
        
        // Apply delay to channel
        for (int channel = 0; channel < mSpec.numChannels; ++channel)
        {
            auto bufferData = buffer.getWritePointer (channel);
            
            // Write from delay buffer to buffer

            if (channel == 0)
            {
                delaySampleL = mDelayLineL.popSample (0) * mMixAmtSmooth.getNextValue();
            }
            
            else if (channel == 1)
            {
                delaySampleR = mDelayLineR.popSample (0) * mMixAmtSmooth.getNextValue();
                applySpread(delaySampleL, delaySampleR);
            }
            
            // buffer.addSample (channel, sample, delaySample);
            
            // Add feedback to buffer
            delaySampleL *= mFeedbackAmtSmooth.getNextValue();
            delaySampleL += bufferData[sample];
            
            delaySampleR *= mFeedbackAmtSmooth.getNextValue();
            delaySampleR += bufferData[sample];
            
            // Write delaySample into mDelayLine
            if (channel == 0)
                mDelayLineL.pushSample (0, delaySampleL);
                
            else if (channel == 1)
                mDelayLineR.pushSample (0, delaySampleR);
        }
        buffer.addSample (0, sample, delaySampleL);
        buffer.addSample (1, sample, delaySampleR);
    }
}

